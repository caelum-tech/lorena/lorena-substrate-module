#![cfg_attr(not(feature = "std"), no_std)]

use support::{decl_module, decl_storage, decl_event, StorageValue, StorageMap, dispatch::Result, ensure};
use system::ensure_signed;
use runtime_primitives::traits::Hash;
use parity_codec::{Encode, Decode};
use rstd::vec::Vec;

pub trait Trait: system::Trait {
    type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>;
}

#[derive(Encode, Decode, Default, Clone, PartialEq)]
#[cfg_attr(feature = "std", derive(Debug))]
pub struct DIDObject<AccountId> {
    owner: AccountId,
    skey: u64,
    zkey: Vec<u64>,
}

decl_event!(
        pub enum Event<T> where
        AccountId = <T as system::Trait>::AccountId,
        Hash = <T as system::Trait>::Hash,
    {
        DidCreated(AccountId, Hash),
    }
);

decl_storage! {
    trait Store for Module<T: Trait> as RadicesModule {
        DidsCount: u32;
        Dids: map T::Hash => DIDObject<T::AccountId>;
        Nonce: u64;
    }
}

decl_module! {
    pub struct Module<T: Trait> for enum Call where origin: T::Origin {
        fn deposit_event<T>() = default;

        pub fn create_did(origin, skey: u64, zkey: Vec<u64>) -> Result {
            let sender = ensure_signed(origin)?;
            let subject = <DidsCount<T>>::get();
            let nonce = <Nonce<T>>::get();
            let did = (<system::Module<T>>::random_seed(), &sender, nonce).using_encoded(<T as system::Trait>::Hashing::hash);

            ensure!(!<Dids<T>>::exists(did), "DID already exists");

            let new_did_object = DIDObject {
                owner: sender.clone(),
                skey: skey,
                zkey: zkey
            };

            <Dids<T>>::insert(did, new_did_object);
            <DidsCount<T>>::put(subject + 1);
            <Nonce<T>>::mutate(|n| *n += 1);

            Self::deposit_event(RawEvent::DidCreated(sender, did));
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {
        use super::*;
        use runtime_io::{with_externalities, TestExternalities};
        use primitives::{H256, Blake2Hasher};
        use support::{impl_outer_origin, assert_ok };
        use runtime_primitives::{
            BuildStorage,
            traits::{BlakeTwo256, IdentityLookup},
            testing::{Digest, DigestItem, Header}
        };    

        impl_outer_origin! {
            pub enum Origin for RadicesTest {}
        }
    
        #[derive(Clone, Eq, PartialEq)]
        pub struct RadicesTest;
    
        impl system::Trait for RadicesTest {
            type Origin = Origin;
            type Index = u64;
            type BlockNumber = u64;
            type Hash = H256;
            type Hashing = BlakeTwo256;
            type Digest = Digest;
            type AccountId = u64;
            type Lookup = IdentityLookup<Self::AccountId>;
            type Header = Header;
            type Event = ();
            type Log = DigestItem;
        }

    
        impl Trait for RadicesTest {
            type Event = ();
        }
    
        type Radices = Module<RadicesTest>;
    
        fn build_ext() -> TestExternalities<Blake2Hasher> {
            let t = system::GenesisConfig::<RadicesTest>::default().build_storage().unwrap().0;
            t.into()
        }

        #[test]
        fn create_did_should_work() {
            with_externalities(&mut build_ext(), || {
                assert_ok!(Radices::create_did(Origin::signed(10), 123, [1, 2, 3].to_vec()));

            })
        }
}