# Lorena Substrate Identity Module

Caelumlabs Identity substrate module.

## Installation

Follow the steps on order to use `lorena-substrate-module` in your blockchain.

### Runtime `Cargo.toml`

Inside `Cargo.toml` file ad the following lines:

```yaml
[features]
default = ['std']
std = [
    ...,
    'lorena-substrate-module/std',
    ...,
]
```

and

```yaml
...
[dependencies.lorena-substrate-module]
default_features = false
git = 'https://gitlab.com/caelum-tech/lorena/lorena-substrate-module.git'
...
```

### Runtime `lib.rs`

Inside the `runtime/src/lib.rs` add the following lines:

Import module:

```yaml
pub use lorena_substrate_module;
```

Add module to runtime:

```rust
impl lorena_substrate_module::Trait for Runtime {
    type Event = Event;
}
```

and add the following to `construct_runtime!` macro:

```rust
...
RadicesModule: lorena_substrate_module::{Module, Call, Storage, Event<T>},
...
```

Check out the [HOWTO](https://github.com/substrate-developer-hub/substrate-module-template) to learn how to use this for your own runtime module.
